﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class PlayerMove : MonoBehaviour {

	private new Rigidbody2D rigidbody;
	

	public Vector2 move;
	public Vector2 velocity;

	public float speed = 20f;
	public float force = 10f;
	public float horizontalSize = 8.3f; //width of game window
	public float verticalSize = 5.4f; //height of game window
	public int score = 0;
	public LayerMask boxLayer;
	public LayerMask asteroidLayer;

	public ParticleSystem explosionPrefab;

	public Text scoreText;

	
 
	void Start () {
		rigidbody = GetComponent <Rigidbody2D> ();
		//rigidbody.useGravity = false;
	}
	void FixedUpdate () {
		Vector2 direction;
		Vector2 newPos;

		direction.x = 0;
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * force * speed;

		float rotationSpeed = 1.5f; 
		rigidbody.AddTorque(-1*Input.GetAxis ("Horizontal") * rotationSpeed);

		rigidbody.AddRelativeForce (velocity * Time.deltaTime);

		if (rigidbody.position.x > horizontalSize) {
			newPos.x = -horizontalSize;
			newPos.y = rigidbody.position.y;
			rigidbody.position = newPos;
		} else if (rigidbody.position.x < -horizontalSize) {
			newPos.x = horizontalSize;
			newPos.y = rigidbody.position.y;
			rigidbody.position = newPos;
		}

		if (rigidbody.position.y > verticalSize) {
			newPos.x = rigidbody.position.x;
			newPos.y = -verticalSize;
			rigidbody.position = newPos;
		} else if (rigidbody.position.y < -verticalSize) {
			newPos.x = rigidbody.position.x;
			newPos.y = verticalSize;
			rigidbody.position = newPos;
		}
	}


	void OnCollisionEnter2D(Collision2D collision){
		//handle crate collision
		if (boxLayer.Contains(collision.gameObject)){
			Destroy(collision.gameObject);
			score = score + 1;

			if (score == 3|| score == 6 || score == 9 || score == 12 || 
				score == 15 || score == 18 || score == 21 || score == 24 || 
				score == 27 || score == 30){

				AsteroidSpawner asteroidSpawn = FindObjectOfType<AsteroidSpawner>();
				asteroidSpawn.SpawnNewAsteroid();
			}

			//Debug.Log("Score: "+ score);
			scoreText.text = score.ToString();

			CrateHandler crateSpawner = FindObjectOfType<CrateHandler>();
       		crateSpawner.SpawnNewCrate();

		}else if(asteroidLayer.Contains(collision.gameObject)){
			ParticleSystem explosion = Instantiate(explosionPrefab);
	        explosion.transform.position = transform.position;
	        Destroy(explosion.gameObject, explosion.duration);
	        
	        GameoverHandler gameoverScreen = FindObjectOfType<GameoverHandler>();
	        gameoverScreen.GameOverTrue(score);
			
			Destroy(gameObject);//hit by asteroid

		}


	}
}
