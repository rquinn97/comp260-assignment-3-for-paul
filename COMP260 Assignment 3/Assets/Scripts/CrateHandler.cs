﻿using UnityEngine;
using System.Collections;

public class CrateHandler : MonoBehaviour {
	public GameObject cratePrefab; 
	public Rigidbody2D playerRigidbody;
	public float xMin,yMin,width,height;
	public int numbCrates = 0;
	// Use this for initialization

	void Start () {
		SpawnNewCrate();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SpawnNewCrate (){
		GameObject crate = Instantiate(cratePrefab);
		crate.transform.parent = transform;
		crate.gameObject.name = "Crate " + numbCrates;
		numbCrates = numbCrates + 1;
		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		Vector2 newPos = new Vector2(x,y);
		//float rot = Random.value * 360;
		if (Mathf.Abs(Vector2.Distance(newPos,playerRigidbody.position))<3){//sqrt(x2-x1)^2 + (y2-y1)^2 oh wait unity does this for me nice
 			x = xMin + Random.value * width;
		 	y = yMin + Random.value * height; //making sure the new crate doesn't spawn too close to the ship
		}
		crate.transform.position = new Vector2(x,y);
		crate.transform.parent = transform;
	}
}
