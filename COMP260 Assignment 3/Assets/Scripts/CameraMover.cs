﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {
	public Rigidbody2D targetRigidbody;
	public GameObject cameraObject; 
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float x = targetRigidbody.position.x;
		float y = targetRigidbody.position.y;

	cameraObject.transform.position = new Vector2(x,y);
	}
}
